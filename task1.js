const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

let output = {};
console.log("loading input file", inputFile);
jsonfile.readFile(inputFile, function (err, body) {
  output.emails = [];
  console.log("iterate all names from", inputFile);
  for (let i = 0; i < body.names.length; i++) {
    let email = "";
    // reverse the name by iterating word from the back, generate 5 random characters and append "@gmail.com"
    for (let j = body.names[i].length - 1; j >= 0; j--) {
      email += body.names[i][j];
    }
    email += randomstring.generate(5) + "@gmail.com";
    // push email to array
    output.emails.push(email);
  }
  console.log("saving output file formatted with 2 space indenting");
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
    console.log("All done!");
  });
});
